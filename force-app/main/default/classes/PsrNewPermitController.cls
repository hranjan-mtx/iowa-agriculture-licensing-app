public class PsrNewPermitController {
    //Method Create new Application Record
    @AuraEnabled
    public static Id addNewApplication(String permitType){
        try{
            User currentUser = [SELECT Id, Contact.AccountId, Contact.Account.Account_Type__c FROM User WHERE Id =: UserInfo.getUserId()];
            Application__c app = new Application__c();
            app.Status__c = 'Draft';
            app.Current_Step__c = currentUser.Contact.Account.Account_Type__c != 'Licensed' ? 1 : 0;
            app.Permit_Type__c = permitType;
            app.Applicant_Account__c = currentUser.Contact.AccountId;
            insert app;
            return app.Id;
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
	
    @AuraEnabled
    public static Boolean isProvider(){
        return [SELECT Contact.Account.Account_Type__c FROM User WHERE Id =: UserInfo.getUserId()].Contact.Account.Account_Type__c == 'Provider';
    }
    
    @AuraEnabled
    public static Boolean isLicensed(){
        return [SELECT Contact.Account.Account_Type__c FROM User WHERE Id =: UserInfo.getUserId()].Contact.Account.Account_Type__c == 'Licensed';
    }

    @AuraEnabled
    public static Integer getCurrentStep(Id appId){
        return (Integer)[SELECT Current_Step__c FROM Application__c WHERE Id =: appId].Current_Step__c;
    }

    @AuraEnabled
    public static String getAppNumber(Id appId){ 
        return (String)[SELECT Name FROM Application__c WHERE Id =: appId].Name;
    }

    @AuraEnabled
    public static Id addAppContact(String AppId, String ContactId){
        try{
            Application_Contacts__c appCon = new Application_Contacts__c();
            appCon.Application__c=AppId;
            appCon.Contact__c=ContactId;
            insert appCon;
            return appCon.Id;
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled
    public static void submitApplication(Id appId){
        try{
           Application__c appRec = [SELECT Id, Status__c FROM Application__c WHERE Id =:appId];
           appRec.Status__c = 'Submitted';
           update appRec; 
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
}