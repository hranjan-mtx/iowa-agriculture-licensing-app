import { LightningElement, track, api } from "lwc";
    import { utility } from "c/pubsub";
    import { ShowToastEvent } from "lightning/platformShowToastEvent";
    import submitApp from "@salesforce/apex/PsrNewPermitController.submitApplication";
    import getAppNo from "@salesforce/apex/PsrNewPermitController.getAppNumber";
    import Portal_Submission_Submission_Message from '@salesforce/label/c.Portal_Submission_Submission_Message'; 

export default class Lp_submission extends LightningElement {

    
    
      @track appId = "";
      @track appNo;
      @api Portal_Submission_Submission_Message = Portal_Submission_Submission_Message;
    
      constructor() {
        super();
        this.appId = utility.getUrlParam("appId");
      }
    
      @api submitApplication() {
        return new Promise((resolve, reject) => {
          this.updateStep();
          console.log("++++submitApplication++++++++++");
          submitApp({ appId: this.appId })
            .then(result => {
              window.console.log("success App Submitted ===>");
              // Show success messsage
              this.dispatchEvent(
                new ShowToastEvent({
                  title: "Success!!",
                  message: "Application Submitted Successfully!!",
                  variant: "success"
                })
              );
              window.open(`/`, "_self");
            })
            .catch(error => {
              this.error = error.message;
            });
          resolve(true);
        });
      }
    
      connectedCallback() {
        getAppNo({ appId: this.appId })
          .then(res => {
            this.appNo = res;
          })
          .catch(err => {
            console.error("error here", JSON.stringify(err));
          });
      }
    }