import {
    LightningElement,api
} from 'lwc';
import footerText from '@salesforce/label/c.Portal_Footer_Text';

export default class Lp_guest_user_footer extends LightningElement {
    @api footerText = footerText;
}