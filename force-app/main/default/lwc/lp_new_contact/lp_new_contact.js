import {LightningElement,api, track} from 'lwc';
// importing to show toast notifictions
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import getContactData from '@salesforce/apex/PsrContactListController.getContact';
import getApplicationContactData from '@salesforce/apex/PsrContactListController.getApplicationContact';
import createApplicationContact  from '@salesforce/apex/PsrContactListController.createContact';
import { utility } from "c/pubsub";
import Portal_Add_New_Contact_Help_Text from '@salesforce/label/c.Portal_Add_New_Contact_Help_Text';

export default class Lp_new_contact extends LightningElement {

    @api isOpenModal = false;
    @api contactId;
    @api applicationId;
    @api applicationContactId;
    @api Portal_Add_New_Contact_Help_Text = Portal_Add_New_Contact_Help_Text;
    @track contact = {};
    accountId;

    get header() {
      return (typeof this.applicationContactId == 'undefined' ||  this.applicationContactId == '' || this.applicationContactId == null) ? 'Add New Contact' : 'Edit Contact';
    }

    connectedCallback(){
      this.applicationId = utility.getUrlParam('appId');
      if(!this.checkBlank(this.applicationContactId)) {
        getApplicationContactData({ applicationContactId: this.applicationContactId})
            .then(result => {
              if(result) {
                this.contact.accountId = result.accountId;
                this.contact.firstName = result.firstName;
                this.contact.lastName = result.lastName;
                this.contact.email = result.email;
                this.contact.phone = result.phone;
                this.contact.companyName = result.companyName;
                this.contact.type = result.type;
                console.log(result.type);
              }
            })
            .catch(err => {
                console.log('error here', JSON.stringify(err));
            })
      }
    }

    handleOpenModal() {
        this.isOpenModal = true;
    }
    handleApplicationSuccess() {
      this.dispatchEvent(new ShowToastEvent({
        title: 'Success!!',
        message: 'Contact Saved Successfully!!',
        variant: 'success'
      }),);
      // Creates the event with the data.
      const selectedEvent = new CustomEvent("closemodal", {
        detail: ''
      });
      // Dispatches the event.
      this.dispatchEvent(selectedEvent);
      this.isOpenModal = false;
    }
    handleSubmit(event) {
      event.preventDefault();
      let fields = event.detail.fields;
      this.contact.contactId = this.contactId;
      this.contact.applicationId = this.applicationId;
      this.contact.id = this.applicationContactId;
      this.contact.firstName = fields.FirstName;
      this.contact.lastName = fields.LastName;
      this.contact.email = fields.Email;
      this.contact.phone = fields.Phone;
      this.contact.companyName = fields.Company_Name__c;
      this.contact.type = fields.Contact_Type__c;
              
      createApplicationContact({ jsonString: JSON.stringify(this.contact) })
          .then(result => {
            if(result) {
              if(result == 'success') {
                this.handleApplicationSuccess(); 
              }
              else {
                this.dispatchEvent(new ShowToastEvent({
                  title: 'Error!!',
                  message: result,
                  variant: 'error'
                }),);
              }
            }
          })
          .catch(err => {
              console.log('error here', JSON.stringify(err));
          })
    }

    handleCloseModal() {
        // Creates the event with the data.
        const selectedEvent = new CustomEvent("closemodal", {
          detail: ''
        });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
        this.isOpenModal = false;
    }
    matchExisting(event) {
      this.retrieveContact(event.detail.value);
    }

    retrieveContact(email) {
      getContactData({ email: email })
          .then(result => {
            if(result) {
              this.contactId = result.Id;
              this.contact.accountId = result.AccountId;
              if(this.checkBlank(this.contact.firstName)) 
                this.contact.firstName = result.FirstName;
              if(this.checkBlank(this.contact.lastName)) 
                this.contact.lastName = result.LastName;
              if(this.checkBlank(this.contact.email)) 
                this.contact.email = result.Email;
              if(this.checkBlank(this.contact.phone)) 
                this.contact.phone = result.Phone;
              if(this.checkBlank(this.contact.companyName)) 
                this.contact.companyName = result.Account.Name;
            }
          })
          .catch(err => {
              console.log('error here', JSON.stringify(err));
          })
  }

  checkBlank(val) {
    if(val != '' && val != null && typeof val != 'undefined') {
      return false;
    }
    return true;
  }
}