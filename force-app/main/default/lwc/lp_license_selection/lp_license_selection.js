import { LightningElement, track, wire ,api } from "lwc";
import saveApp from '@salesforce/apex/PsrNewPermitController.addNewApplication';
import Portal_Installation_Help_Text from '@salesforce/label/c.Portal_Installation_Help_Text';
import Portal_Foundation_Help_Text from '@salesforce/label/c.Portal_Foundation_Help_Text';
import Portal_Machanical_Help_Text from '@salesforce/label/c.Portal_Machanical_Help_Text';
import Portal_Electrical_Help_Text from '@salesforce/label/c.Portal_Electrical_Help_Text';
import Portal_Repair_Help_Text from '@salesforce/label/c.Portal_Repair_Help_Text';
import Portal_Refurbish_Help_Text from '@salesforce/label/c.Portal_Refurbish_Help_Text';
import Portal_Cancel_Permit_Help_Text from '@salesforce/label/c.Portal_Cancel_Permit_Help_Text';
import Portal_Request_Change_of_Address_Help_Text from '@salesforce/label/c.Portal_Request_Change_of_Address_Help_Text';
import Portal_Request_Duplicate_Permit_Help_Text from '@salesforce/label/c.Portal_Request_Duplicate_Permit_Help_Text';

export default class Lp_license_selection extends LightningElement {
    @api Portal_Installation_Help_Text = Portal_Installation_Help_Text;
    @api Portal_Foundation_Help_Text = Portal_Foundation_Help_Text;
    @api Portal_Machanical_Help_Text = Portal_Machanical_Help_Text;
    @api Portal_Electrical_Help_Text = Portal_Electrical_Help_Text;
    @api Portal_Repair_Help_Text = Portal_Repair_Help_Text;
    @api Portal_Refurbish_Help_Text = Portal_Refurbish_Help_Text;
    @api Portal_Cancel_Permit_Help_Text = Portal_Cancel_Permit_Help_Text;
    @api Portal_Request_Duplicate_Permit_Help_Text = Portal_Request_Duplicate_Permit_Help_Text;
    @api Portal_Request_Change_of_Address_Help_Text = Portal_Request_Change_of_Address_Help_Text;

    handleNewPermit(event) {
        let _appId;
        let permitType = event.currentTarget.dataset.permitType;
        
        saveApp({permitType : permitType})
         .then(result =>{
            window.console.log('result ===> '+result);
            _appId = result;
            window.open(`/s/new-permit?appId=${_appId}`, "_self");
            // Show success messsage
            // this.dispatchEvent(new ShowToastEvent({
            //     title: 'Success!!',
            //     message: 'New Application Created Successfully!!',
            //     variant: 'success'
            // }),);
        })
        .catch(error => {
            this.error = error.message;
        });
    }

    cancelPermit(){
        window.open(`/s/cancel-permit`, "_self");
    }

    requestDuplicatePermit(){
        window.open(`/s/request-duplicate-permit`, "_self");
    }

    requestChangeOfAddress(){
        window.open(`/s/request-address-change`, "_self");
    }
}