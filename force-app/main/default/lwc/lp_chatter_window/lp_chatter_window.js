/**
 * Created by hardikranjan on 03/05/20.
 */

import { LightningElement, api } from 'lwc';
import lpCommunityApexBaseUrl from '@salesforce/label/c.Portal_vfpage_url';


export default class lp_chatter_window extends LightningElement {
    @api recordId = "";
    @api headingLabel = "";

    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    get fullUrl() {
        return lpCommunityApexBaseUrl+ 'LpChatterPage?id=' + this.recordId;
    }

}