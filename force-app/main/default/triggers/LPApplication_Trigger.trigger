trigger LPApplication_Trigger on Application__c (after insert, after update, before update) {

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ApplicationTriggerHandler.createDueDiligence(Trigger.new);
            // added method from PSRApplication_Trigger
            ApplicationTriggerHandler.insertApplicationDocumentRecords(Trigger.new);
            ApplicationTriggerHandler.insertApplicationContacts(Trigger.new);
        }

        if (Trigger.isUpdate) {
//            ApplicationTriggerHandler.sendNotification(Trigger.new);
            ApplicationTriggerHandler.updateLicenseInformation(Trigger.new, Trigger.oldMap);
        }
    }

    if (Trigger.isBefore && Trigger.isUpdate) {
        ApplicationTriggerHandler.populateAgencyStaffandInspector(Trigger.new);
        ApplicationTriggerHandler.updateDueDiligenceStatus(Trigger.new);

    }

}